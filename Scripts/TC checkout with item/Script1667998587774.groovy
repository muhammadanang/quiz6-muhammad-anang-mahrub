import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.urlDemo)

WebUI.setText(findTestObject('login/input_username'), username)

WebUI.setText(findTestObject('login/input_password'), password)

WebUI.click(findTestObject('login/button_login'))

WebUI.click(findTestObject('products/backpack'))

WebUI.click(findTestObject('cart/button_cart'))

WebUI.click(findTestObject('cart/button_checkout'))

WebUI.setText(findTestObject('cart/input_first_name'), firstname)

WebUI.setText(findTestObject('cart/input_last_name'), lastname)

WebUI.setText(findTestObject('cart/input_pos'), postal_code)

WebUI.click(findTestObject('cart/button_continue'))

WebUI.click(findTestObject('cart/button_finish'))

WebUI.verifyElementPresent(findTestObject('cart/txt_checkout_complete'), 5)

WebUI.closeBrowser()
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.urlDemo)

WebUI.setText(findTestObject('login/input_username'), username)

WebUI.setText(findTestObject('login/input_password'), password)

WebUI.click(findTestObject('login/button_login'))

if(number == 'first') {
	WebUI.click(findTestObject('products/fleece_jacket'))
	WebUI.click(findTestObject('products/onesie'))
	WebUI.verifyElementText(findTestObject('cart/num_items_in_cart'), '2', FailureHandling.STOP_ON_FAILURE)
} else if(number == 'two') {
	WebUI.click(findTestObject('products/backpack'))
	WebUI.click(findTestObject('products/bike_light'))
	WebUI.click(findTestObject('products/bolt_t_shirt'))
	WebUI.click(findTestObject('products/fleece_jacket'))
	WebUI.click(findTestObject('products/onesie'))
	WebUI.click(findTestObject('products/t_shirt_(red)'))
	WebUI.verifyElementText(findTestObject('cart/num_items_in_cart'), '6', FailureHandling.STOP_ON_FAILURE)
} else if(number == 'three') {
	WebUI.click(findTestObject('products/backpack'))
	WebUI.click(findTestObject('products/remove_backpack'))
	WebUI.verifyElementNotPresent(findTestObject('cart/num_items_in_cart'), 3)
}

WebUI.closeBrowser()
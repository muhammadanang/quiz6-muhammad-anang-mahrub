<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>remove_backpack</name>
   <tag></tag>
   <elementGuidId>c7533338-35eb-4a4d-9a51-051772ef509b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'remove-sauce-labs-backpack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>remove-sauce-labs-backpack</value>
      <webElementGuid>01fd3553-70b2-442e-92f6-7628e3cfba62</webElementGuid>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>bolt_t_shirt</name>
   <tag></tag>
   <elementGuidId>81a677d4-1e2d-4d2a-ab57-7ffc7701b4e4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'add-to-cart-sauce-labs-bolt-t-shirt']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-bolt-t-shirt</value>
      <webElementGuid>a7209150-ed12-4d03-a253-8ebdf60258a3</webElementGuid>
   </webElementProperties>
</WebElementEntity>

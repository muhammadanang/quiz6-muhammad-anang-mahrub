<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>onesie</name>
   <tag></tag>
   <elementGuidId>ae4fa433-37dd-4da7-be67-b2ee886712c4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'add-to-cart-sauce-labs-onesie']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-onesie</value>
      <webElementGuid>7eefa3bb-3478-4c1e-9a83-7e383a732b21</webElementGuid>
   </webElementProperties>
</WebElementEntity>

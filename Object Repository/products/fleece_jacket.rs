<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>fleece_jacket</name>
   <tag></tag>
   <elementGuidId>1b0e3183-d325-4d5b-ac36-0298074afa86</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'add-to-cart-sauce-labs-fleece-jacket']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>add-to-cart-sauce-labs-fleece-jacket</value>
      <webElementGuid>61bbad4c-97ec-485a-895b-27fc2c2d91f7</webElementGuid>
   </webElementProperties>
</WebElementEntity>

<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>t_shirt_(red)</name>
   <tag></tag>
   <elementGuidId>7330c970-4b6e-462d-b827-d7dae8cb4f57</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 'add-to-cart-test.allthethings()-t-shirt-(red)']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>add-to-cart-test.allthethings()-t-shirt-(red)</value>
      <webElementGuid>4026a9fd-e091-4ca0-8d67-2431dd8fb22c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
